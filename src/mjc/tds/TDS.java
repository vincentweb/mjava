package mjc.tds;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import mjc.type.CLASSE;
import mjc.type.DTYPE;

public class TDS<E extends INFO> extends HashMap<String, E> {

	private static final long serialVersionUID = 1L;

	private TDS<E> parente;

	public TDS() {
		this(null);
	}

	public TDS(TDS<E> p) {
		super();
		parente = p;
	}
	
	public TDS<E> getParente() {
	    return this.parente;
	}

	public E chercherLocalement(String n) {
		return get(n);
	}

	public E chercherGlobalement(String n) {
		E i = chercherLocalement(n);
		if (i == null)
			if (parente != null)
				return parente.chercherGlobalement(n);
		return i;
	}

	public void inserer(String n, E i) {
		put(n, i);
	}

	// ça n'a rien à faire ici !!!!!!!
	// FIXME: interdire les mainS avec des paramètres !!
	public INFOMETHODE unEtUnSeulMain(){
	    TDMETH methodes;
	    DTYPE classe;
	    INFOMETHODE candidat = null;
	    INFOMETHODE resultat = null;
	    int nb;
	    TDS<E> tds = this;
	    while(tds.getParente() != null) {
	    	tds = tds.getParente();
	    }
	    for(E info : tds.values()) {
			if(info instanceof INFOTYPE) {
			    classe = ((INFOTYPE) info).getType();
			    if(classe instanceof CLASSE) {
				    	methodes = ((CLASSE) classe).getMethodes();
				    	candidat = methodes.unEtUnSeulMain();
					if(candidat != null){
					    if(resultat != null) { // plus d'un main
					    	System.out.println("il y a plus qu'un main");
					    	return null;
					    } 
					    else {
					    	resultat = candidat;
					    }
					}
			    }
			}
	    }
	    if (resultat.getArguments().size() > 0 || !(resultat.getTypeRetour().getNom().equals("void")) )
	    	return null;
    	else
    		return resultat;
	}

	public String toString() {
//		return "td";
		StringBuffer sb = new StringBuffer();
		Set<Map.Entry<String, E>> s = entrySet();
		for (Map.Entry<String, E> e : s){
			sb.append("; " + e.getKey() + " : " + e.getValue() + '\n');
		
		}
		if (this.parente != null)
			sb.append(parente.toString());
		return sb.toString();
	}
	
	public boolean OPUNCoherent(String op, DTYPE type1) {
		if(op.equals("")) {
			if(type1.getNom().equals("int")) {
				return true;
			} else if (type1.getNom().equals("bool")) {
				return false;
			} else return false;
		} else if (op.equals("INeg")) {
			if(type1.getNom().equals("int")) {
				return true;
			} else if (type1.getNom().equals("bool")) {
				return false;
			} else return false;
		} else if (op.equals("BNeg")) {
			if(type1.getNom().equals("int")) {
				return false;
			} else if (type1.getNom().equals("bool")) {
				return true;
			} else return false;
		} else return false;
	}
	
	public boolean OPMULCoherent(String op, DTYPE type1, DTYPE type2) {
		if(type2 != null) {
			if(!type1.getNom().equals(type2.getNom())) {
				return false;
			}
		}
	
		if(op.equals("IMul")) {
			if(type1.getNom().equals("int")) {
				return true;
			} else if (type1.getNom().equals("bool")) {
				return false;
			} else return false;
		} else if (op.equals("IDiv")) {
			if(type1.getNom().equals("int")) {
				return true;
			} else if (type1.getNom().equals("bool")) {
				return false;
			} else return false;
		} else if (op.equals("IMod")) {
			if(type1.getNom().equals("int")) {
				return true;
			} else if (type1.getNom().equals("bool")) {
				return false;
			} else return false;
		} else if (op.equals("BAnd")) {
			if(type1.getNom().equals("int")) {
				return false;
			} else if (type1.getNom().equals("bool")) {
				return true;
			} else return false;
		} else return false;
		
	}
	
	public boolean OPADDCoherent(String op, DTYPE type1, DTYPE type2) {
		if(type2 != null) {
			if(!type1.getNom().equals(type2.getNom())) {
				return false;
			}
		}
		
		if(op.equals("IAdd")) {
			if(type1.getNom().equals("int")) {
				return true;
			} else if (type1.getNom().equals("bool")) {
				return false;
			} else return false;
		} else if (op.equals("ISub")) {
			if(type1.getNom().equals("int")) {
				return true;
			} else if (type1.getNom().equals("bool")) {
				return false;
			} else return false;
		} else if (op.equals("BOr")) {
			if(type1.getNom().equals("int")) {
				return false;
			} else if (type1.getNom().equals("bool")) {
				return true;
			} else return false;
		} else return false;
		
	}
	
	public boolean OPRELCoherent(String op, DTYPE type1) {
		if(op.equals("ILss")) {
			if(type1.getNom().equals("int")) {
				return true;
			} else if (type1.getNom().equals("bool")) {
				return false;
			} else return false;
		} else if (op.equals("ILeq")) {
			if(type1.getNom().equals("int")) {
				return true;
			} else if (type1.getNom().equals("bool")) {
				return false;
			} else return false;
		} else if (op.equals("IGtr")) {
			if(type1.getNom().equals("int")) {
				return true;
			} else if (type1.getNom().equals("bool")) {
				return false;
			} else return false;
		} else if (op.equals("IGeq")) {
			if(type1.getNom().equals("int")) {
				return true;
			} else if (type1.getNom().equals("bool")) {
				return false;
			} else return false;
		} else if (op.equals("IEq")) {
			if(type1.getNom().equals("int")) {
				return true;
			} else if (type1.getNom().equals("bool")) {
				return true;
			} else if (type1.getNom().equals("Null")) {
				return true;
			} else return false;
		} else if (op.equals("INeq")) {
			if(type1.getNom().equals("int")) {
				return true;
			} else if (type1.getNom().equals("bool")) {
				return true;
			} else return false;
		} else return false;
	}
		
}
