package mjc.tds;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import mjc.type.ARGUMENT;
import mjc.type.CLASSE;
import mjc.type.DTYPE;

public class TDMETH extends ArrayList<INFOMETHODE> {
	
    private static final long serialVersionUID = 1557925775756842144L;
    
    TDMETH parente;

    public TDMETH() {
    	this(null);
    }

    public TDMETH(TDMETH parente) {
    	super();
    	this.parente = parente;
    }
    
    
    
    /**
     * renvoie la première instance de INFOMETHODE correspondant au nom et aux arguments
     * fournis, présent dans METHODES.
     */
    public INFOMETHODE chercherLocalement (String nom, List<ARGUMENT> arguments) {
		INFOMETHODE result = null;
		INFOMETHODE tmp = null;
		Iterator<INFOMETHODE> it = this.iterator();
		while(it.hasNext() && result == null) {
		    tmp = it.next();
		    if(tmp.getNom().equals(nom) && tmp.getArguments().equals(arguments)) {
			    result = tmp;
		    }
		}
		return result;
    }
    
    /*
     * Renvoie la méthode qui satisfait la signature imeth décrite par une interface
     */
    public INFOMETHODE chercherGlobalementInterf(INFOMETHODE imeth){
    	INFOMETHODE meth = this.chercherLocalement(imeth.getNom(), imeth.getArguments());
    	if (meth == null){
    		if (parente == null){
    			return null;
    		}
    		else {
    			return parente.chercherGlobalementInterf(imeth);
    		}
    	}
    	else if (meth.getTypeRetour().compareTo(imeth.getTypeRetour())){
    		return meth;
    	}
    	else {
    		return null;
    	}
    }
    
    public TDMETH getParent(){
    	return this.parente;
    }
    


    public INFOMETHODE chercherOptimum (String nom, List<DTYPE> arguments) throws Exception {
    	// On chercher une méthode meth tq meth.getNom() == nom && arguments ≤ meth.arguments
    	// mais on veut celle qui ait les arguments avec la distance la plus petite possible 
    	ArrayList<INFOMETHODE> candidats = new ArrayList<INFOMETHODE>(); // nécéssaire pour le faire à la Java (cf exemples plus haut)
    	TDMETH This = this;
    	boolean argumentsOK;
    	// utiliser une rechercheLocaleOptimale pourrait échouer, cf exemples Java
    	while (This != null) {
	    	for (INFOMETHODE meth : This){
	    		// arguments ≤ meth.arguments <=> argumentsOK = true
	    		argumentsOK = true;
	    		if (arguments.size() != meth.getArguments().size()) {
	    			argumentsOK = false;
	    		}
	    		else {
		    		for (int i=0 ; i<arguments.size(); i++){
		    			if (!(arguments.get(i).compareTo(meth.getArguments().get(i).getType())))
		    				argumentsOK = false;
		    		}
	    		}
	    		if (meth.getNom().equals(nom) && argumentsOK){
	    			candidats.add(meth);
	    		}
	    	}
	    	This = This.getParent();
    	}
    	// là on a les candidats de tous les parents, maintenant, faut prendre le meilleur, et détecter les “ambigous”
    	
    	// si on a pas de candidat, on renvoie null
    	if (candidats.size() == 0)
    	{
    		System.out.println("DEBUG : rechercherOptimale, pas de candidats.");
    		return null;
    	}
    	// chercher le meilleur, 
	    //stratégie : enlever les moins bons, si on les enlève tous, c’est qu’il y avait une ambiguité
	
	    // on itère sur les arguments, puis sur les méthodes
    	@SuppressWarnings("unchecked")
		ArrayList<INFOMETHODE> candidatFinaux = (ArrayList<INFOMETHODE>) candidats.clone();
    	ArrayList<INFOMETHODE> candidats2 = new ArrayList<INFOMETHODE>();
	    for (int i=0 ; i<arguments.size() ; i++) {
	    	// déterminer les meilleures méthodes pour le ième argument
	    	int dist = 100; // 100 = l’infini
	    	int ndist;
	    	candidats2 = new ArrayList<INFOMETHODE>(); // les candidats que l'on garde
	    	for (INFOMETHODE meth : candidats) {
	    		ndist = arguments.get(i).distanceDe(meth.getArguments().get(i).getType());
	    		if (ndist!=-1 && ndist < dist) {
	    			candidats2 = new ArrayList<INFOMETHODE>(); // effacer candidats2
	    			candidats2.add(meth);
	    			dist = ndist;
	    		}
	    		else if (ndist == dist)
	    			candidats2.add(meth);			
	    	}
	    	candidatFinaux.retainAll(candidats2); // on garde l'intersection des meilleurs
	    }
	    if (candidatFinaux.size() != 1) {
	    	System.out.println("Ambiguité between "+candidatFinaux);
	    	throw new Exception("Ambiguité entre le choix de deux méthodes."); // ou return null
	    }
	    return candidatFinaux.get(0);
    
    }
    
    public void inserer(INFOMETHODE meth, CLASSE classe){
    	TDMETH This = this;
    	while (This!=null) {
    		INFOMETHODE methrech = This.chercherLocalement(meth.getNom(), meth.getArguments());
    		if ( methrech != null ) {
    			This.remove(methrech);
    		}
    		This = This.getParent();
    	}
    	if (classe != null) {
    		meth.setClasse(classe);
    	}
    	this.add(meth);
    }

    public INFOMETHODE unEtUnSeulMain() {
		INFOMETHODE candidat = null;
		for(INFOMETHODE methode : this) {
		    if(methode.getNom().equals("main")) {
		    	if (candidat == null) {
		    		candidat = methode;
		    	} 
		    	else {
		    		System.out.println("il y a plus qu'un main");
		    		return null;
		    	}
		    }
		}
		return candidat;
    }
    
    public String toString() {
    	String s = "";
    	for(INFOMETHODE methode : this) {
        	s += "\t\t"+methode+"\n";
        }
    	if (parente != null) {
    		System.out.println("-- méthodes héritées --");
    		s += parente.toString();
    	}
    	return s;
    }

}
