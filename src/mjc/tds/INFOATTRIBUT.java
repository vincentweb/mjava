package mjc.tds;

import mjc.type.DTYPE;
// En faite INFOATTRIBUT == INFOTYPE, c'est juste le nom et le cas d'utilisation qui change
public class INFOATTRIBUT extends INFO {

	int deplacement;
	
	public INFOATTRIBUT(DTYPE t) {
		super(t);
		
	}
	
	public void setDeplacement(int deplacement) {
		this.deplacement = deplacement;
	}
	
	public int getDeplacement() {
		return this.deplacement;
	}
}
