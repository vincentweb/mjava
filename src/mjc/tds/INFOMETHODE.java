package mjc.tds;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import mjc.type.ARGUMENT;
import mjc.type.CLASSE;
import mjc.type.DTYPE;
import mjc.type.PARAMETRES;



public class INFOMETHODE extends INFO {
	
    private DTYPE typeRetour;
    private PARAMETRES arguments;
    private String nom;
	private int taille; // FIXME wtf ?
	private int numero;
	private CLASSE classe;
    

    
    public INFOMETHODE(DTYPE typeRetour, String nom, PARAMETRES arguments) {
		super(typeRetour); // à voir
		this.typeRetour = typeRetour;
		this.nom =  nom;
		this.arguments = arguments;
		this.taille = 0; // A setter à la fin de la declaration des methodes ...
	}
	
    public DTYPE getTypeRetour(){
    	return typeRetour;
    }
    
    public PARAMETRES getArguments(){
    	return arguments;
    }

    public void ajouterArgument(ARGUMENT argument){
    	arguments.inserer(argument);
    }
    
    public boolean argumentsValides(List<DTYPE> types){
    	if (types.size() != arguments.size())
    		return false;
    	Iterator<DTYPE> itTypes = types.iterator();
    	Iterator<ARGUMENT> itArgs  = arguments.iterator();
    	while (itTypes.hasNext() && itArgs.hasNext()){
    		if (! itTypes.next().compareTo(itArgs.next().getType()))
    			return false;
    	}
    	return true;
    }
    
    public String getNom() {
    	return this.nom;
    }
    
    public int getNumero() {
    	return this.numero;
    }
    
    public void setNumero(int numero) {
    	this.numero = numero;
    }
    
    public String getLabel() {
    	return  String.valueOf(classe.getNumeroClasse())+"_"+ String.valueOf(this.numero);
    }
    
    public void setClasse(CLASSE classe) {
    	this.classe = classe;
    }
    
    public CLASSE getClasse() {
    	return this.classe;
    }
    
    public boolean compareTo(INFOMETHODE methode){
    	// J'ai fait des tests en Java, on est pas obligé de respecter directo-stricto les interfaces qu'on implémente
    	/* exemple qui marche/compile
    	import java.util.List;
    	import java.util.Vector;

    	public interface Interf {
    		List fun(int toto);
    		Vector fun2();
    	}
    	
    	import java.util.ArrayList;
    	import java.util.List;
    	import java.util.Stack;
    	import java.util.Vector;

		* classe.java
    	public class classe implements Interf{

    		public ArrayList fun(int tata){
    			// toto != tata mais Java s'en moque !
    			// ArrayList implémente List
    			return new ArrayList();
    		}
    		public Stack fun2(){
    			// Stack est une classe fille de Vector
    			return new Stack();
    		}
    		
    		public static void main(String[] args) 
    	    {
    			System.out.println("iw");
    	    }
    	}
    	 */
    	if (methode.getArguments().size() != arguments.size())
    		return false;
    	
    	Iterator<ARGUMENT> itArgs  = arguments.iterator();
    	Iterator<ARGUMENT> itArgs2 = methode.getArguments().iterator();
    	while (itArgs2.hasNext() && itArgs.hasNext()){
    		if (! itArgs.next().getType().compareTo(itArgs2.next().getType()))
    			return false;
    	}
    	return typeRetour.compareTo(methode.getTypeRetour());
    }
    
    public boolean equals(Object infometh) {
    	if (infometh==this) {
            return true;
        }
        if (infometh instanceof INFOMETHODE) {
        	return ((INFOMETHODE)infometh).getNom().equals(nom) && ((INFOMETHODE)infometh).getArguments().equals(arguments) && ((INFOMETHODE)infometh).getTypeRetour().equals(typeRetour);
        }
        return false;
    }
    
    public int hashCode() {
    	return nom.hashCode() + typeRetour.hashCode()*5 + arguments.hashCode()*25;
    }
    
    // TODO: corriger this.arguments.toString()
    public String toString() {
//    	return "infometh";
        String s =  "INFOMETHODE : " + nom + " (" + this.classe.getNom() +")";
        
    	s+= "\n\t typeRetour=" + this.typeRetour.getNom() + " ("+this.typeRetour.getClass().toString()+")\n";
    	s+=  " \targuments="+this.arguments ;
        return s;
    }

	public int getTaille() {
		return this.taille;
	}
	
	public void setTaille(int taille) {
		this.taille = taille;
	}

    
}
