package mjc.tds;

import java.util.ArrayList;

import mjc.type.CLASSE;
import mjc.type.DTYPE;
import mjc.type.INTERFACE;
import mjc.type.ARGUMENT;
import mjc.type.PARAMETRES;

public class TdsTest {
	
	public static void main(String[] args) throws Exception{
		
		/* exemple qui marche/compile
    	import java.util.List;
    	import java.util.Vector;

    	public interface Interf {
    		List fun(int toto);
    		Vector fun2();
    	}
    	
    	import java.util.ArrayList;
    	import java.util.List;
    	import java.util.Stack;
    	import java.util.Vector;

		* classe.java
    	public class classe implements Interf{

    		public ArrayList fun(int tata){
    			// toto != tata mais Java s'en moque !
    			// ArrayList implémente List
    			return new ArrayList();
    		}
    		public Stack fun2(){
    			// Stack est une classe fille de Vector
    			return new Stack();
    		}
    		
    		public static void main(String[] args) 
    	    {
    			System.out.println("iw");
    	    }
    	}
    	 */
		
		DTYPE vide = new DTYPE("void", 0);
		INTERFACE list = new INTERFACE("List");
		CLASSE arrayList = new CLASSE("ArrayList");
		CLASSE vector = new CLASSE("Vector");
		CLASSE stack = new CLASSE("Stack");
		CLASSE a = new CLASSE("A");
		CLASSE b = new CLASSE("B");
		CLASSE c = new CLASSE("C");
		
		CLASSE classe = new CLASSE("Classe");
		INTERFACE interf = new INTERFACE("Interf");
		
		classe.addInterface(interf);
		b.addParent(a);
		c.addParent(b);
		
		stack.addParent(vector);
		arrayList.addInterface(list);
		vector.addInterface(list);
		
		PARAMETRES argus = new PARAMETRES();
		argus.add(new ARGUMENT(new DTYPE("void",0),"non"));
		
		PARAMETRES argus2 = new PARAMETRES();
		argus2.add(new ARGUMENT(new DTYPE("void",0),"non"));

		
		interf.addMethode(new INFOMETHODE(list, "fun", argus));
		interf.addMethode(new INFOMETHODE(vector, "fun", argus2));
		classe.addMethode(new INFOMETHODE(arrayList, "fun", argus));
		classe.addMethode(new INFOMETHODE(stack, "fun", argus2));
		
		System.out.println("stack <= vector (true)"+ stack.compareTo(vector));
		System.out.println("arrayList <= List (true)"+ arrayList.compareTo(list));
		System.out.println("Interface respectée (true) : "+classe.interfaceRespectee());
		


//		A JUNITER :
//		System.out.println("distance list-stack "+list.distanceDe(stack));
		
		CLASSE Object = new CLASSE("Object"); 
		INTERFACE IGeo = new INTERFACE("IGeo");
		INTERFACE IPoint = new INTERFACE("IPoint");
		CLASSE Point = new CLASSE("Point");
		IPoint.addInterfParente(IGeo);
		Point.addInterface(IPoint);
		Point.addParent(Object);
		
		DTYPE Int = new DTYPE("int", 1);
		
		IGeo.addMethode(new INFOMETHODE(Int, "getNumero", new PARAMETRES()));
		Point.addMethode(new INFOMETHODE(Int, "getNumero", new PARAMETRES()));
		
		IPoint.addMethode(new INFOMETHODE(Int, "getX", new PARAMETRES()));
		Point.addMethode(new INFOMETHODE(Int, "getX", new PARAMETRES()));
		
		System.out.println("Interf respectée (true) "+Point.interfaceRespectee());
		
		
		PARAMETRES p1 = new PARAMETRES();
		PARAMETRES p2 = new PARAMETRES();
		p1.add(new ARGUMENT(Int, "l"));
		p2.add(new ARGUMENT(Int, "x"));
		System.out.println("cmp "+p1.equals(p2));
		
		System.out.println("TEST : "+new DTYPE("int", 0).distanceDe(new DTYPE("int", 0)));
		
	}
}
