package mjc.tds;

import static org.junit.Assert.*;

import java.util.ArrayList;

import mjc.type.ARGUMENT;
import mjc.type.CLASSE;
import mjc.type.DTYPE;
import mjc.type.INTERFACE;
import mjc.type.PARAMETRES;

import org.junit.Before;
import org.junit.Test;

public class TDMETH_JTest {
	
	public static void main(String[] args) {
		org.junit.runner.JUnitCore.main(TDMETH_JTest.class.getName());
	}
	
	DTYPE vide;
	INTERFACE list;
	CLASSE arrayList;
	CLASSE vector;
	CLASSE stack;
	CLASSE a;
	CLASSE b;
	CLASSE c;
	INFOMETHODE af1;
	INFOMETHODE bf1;
	INFOMETHODE bf1_2;
	INFOMETHODE cf1;
	PARAMETRES aargf1;
	PARAMETRES bargf1;
	PARAMETRES bargf1_2;
	PARAMETRES cargf1;
	ArrayList<DTYPE> param;
	
	@Before
	public void setUp() {
		vide = new DTYPE("void", 0);
		list = new INTERFACE("List");
		arrayList = new CLASSE("ArrayList");
		vector = new CLASSE("Vector");
		stack = new CLASSE("Stack");
		a = new CLASSE("A");
		b = new CLASSE("B");
		c = new CLASSE("C");
		
		b.addParent(a);
		c.addParent(b);
		
		stack.addParent(vector);
		arrayList.addInterface(list);
		vector.addInterface(list);
		
		aargf1 = new PARAMETRES();
		bargf1 = new PARAMETRES();
		bargf1_2 = new PARAMETRES();
		cargf1 = new PARAMETRES();
		param = new ArrayList<DTYPE>();
		
		af1 = new INFOMETHODE(vide, "f1", aargf1);
		bf1 = new INFOMETHODE(vide, "f1", bargf1);
		bf1_2 = new INFOMETHODE(vide, "f1", bargf1_2);
		cf1 = new INFOMETHODE(vide, "f1", cargf1);
		
		a.addMethode(af1);
		b.addMethode(bf1);
		c.addMethode(cf1);
		
	}

	@Test
	public void testChercherOptimum1() throws Exception {
		/* ChercherOptimum doit renvoyer A.f1
		 * 
		 * public class A{
				public void f1(List a, Stack b){
					System.out.println("A : f1");
				}
				
			}
			
			public class B extends A{
				public void f1(List a, Vector b){
					System.out.println("B : f1");
				}
			}
			
			public classe(){
				B b = new B();
				b.f1(new Stack(), new Stack());
			}
		 */
		aargf1.add(new ARGUMENT(list, "a"));
		aargf1.add(new ARGUMENT(stack, "b"));
		bargf1.add(new ARGUMENT(list, "a"));
		bargf1.add(new ARGUMENT(vector, "b"));
		param.add(stack);
		param.add(stack);
		assertTrue(b.getMethodes().chercherOptimum("f1", param).equals(af1));
	}
	
	@Test
	public void testChercherOptimum2() {
		/* ChercherOptimum doit renvoyer une exception
		 * 
		 * public class A{
			public void f1(Stack a, List b){
				System.out.println("A : f1");
			}
			
		}
		
		public class B extends A{
			public void f1(List a, Vector b){
				System.out.println("B : f1");
			}
		}
		
		public classe(){
			B b = new B();
			b.f1(new Stack(), new Stack());
		}
		 */
		aargf1.add(new ARGUMENT(stack, "a"));
		aargf1.add(new ARGUMENT(list, "b"));
		bargf1.add(new ARGUMENT(list, "a"));
		bargf1.add(new ARGUMENT(vector, "b"));
		param.add(stack);
		param.add(stack);
		try {
			b.getMethodes().chercherOptimum("f1", param).equals(af1);
			fail("Fallait une exception");
		} catch (Exception e) {
		}
	}
	
	@Test
	public void testChercherOptimum3() throws Exception {
		/* ChercherOptimum doit renvoyer A.f1 (si A n'existait pas, on aurait eu une ambiguité -> impossible de coder la recherche optimale récursivement)
		 * 
		 *  public class A{
				public void f1(Stack a, Stack b){
					System.out.println("A : f1");
				}
				
			}
			
			public class B extends A{
				public void f1(Stack a, List b){
					System.out.println("B : f1");
				}
			}
			
			public class C extends B{
				public void f1(List a, Vector b){
					System.out.println("C : f1");
				}
			}
			
			public classe(){
				C c = new C();
				c.f1(new Stack(), new Stack());
			}
		 */
		aargf1.add(new ARGUMENT(stack, "a"));
		aargf1.add(new ARGUMENT(stack, "b"));
		bargf1.add(new ARGUMENT(stack, "a"));
		bargf1.add(new ARGUMENT(list, "b"));
		cargf1.add(new ARGUMENT(list, "a"));
		cargf1.add(new ARGUMENT(vector, "b"));
		param.add(stack);
		param.add(stack);
		assertTrue(b.getMethodes().chercherOptimum("f1", param).equals(af1));
	}
	
	@Test
	public void testChercherOptimum4() {
		/* ChercherOptimum doit renvoyer une exception
		 * 
		 *  public class A{
				public void f1(Vector a, Stack b){
					System.out.println("A : f1");
				}
			
			}
			
			public class B extends A{
				public void f1(Stack a, List b){
					System.out.println("B : f1");
				}
			}
			
			public class C extends B{
				public void f1(List a, Vector b){
					System.out.println("C : f1");
				}
			}
			
			public classe(){
				C c = new C();
				c.f1(new Stack(), new Stack());
			}
		 */
		aargf1.add(new ARGUMENT(vector, "a"));
		aargf1.add(new ARGUMENT(stack, "b"));
		bargf1.add(new ARGUMENT(stack, "a"));
		bargf1.add(new ARGUMENT(list, "b"));
		cargf1.add(new ARGUMENT(list, "a"));
		cargf1.add(new ARGUMENT(vector, "b"));
		param.add(stack);
		param.add(stack);
		try {
			b.getMethodes().chercherOptimum("f1", param).equals(af1);
			fail("ON aurait du voir une exception !");
		} catch (Exception e) {
		}
	}
	
	@Test
	public void testChercherOptimum5() throws Exception {
		/* ChercherOptimum doit renvoyer A.f1 (si A n'existait pas, on aurait eu une ambiguité -> impossible de coder la recherche optimale récursivement)
		 * Cette exemple compile malgrès l’ambiguité dans la classe B qui n’est pas levée, cet exemple prouve pourquoi on ne peut coder rechercherGlobalementOptimum en fonction de rechercherLocalementOptimum
		 * 
		 *  public class A{
				public void f1(Vector a, Stack b){
					System.out.println("A : f1");
				}
				
				
			}
			
			public class B extends A{
				public void f1(Vector a, List b){
					System.out.println("B : f1");
				}
				public void f1(List a, Vector b){
					System.out.println("B : f1");
				}
			}
			
			public class C extends B{
				public void f1(List a, Stack b){
					System.out.println("C : f1");
				}
			}
			
			public classe(){
				C c = new C();
				c.f1(new Stack(), new Stack());
			}
		 */
		aargf1.add(new ARGUMENT(vector, "a"));
		aargf1.add(new ARGUMENT(stack, "b"));
		bargf1.add(new ARGUMENT(vector, "a"));
		bargf1.add(new ARGUMENT(list, "b"));
		bargf1_2.add(new ARGUMENT(list, "a"));
		bargf1_2.add(new ARGUMENT(vector, "b"));
		cargf1.add(new ARGUMENT(list, "a"));
		cargf1.add(new ARGUMENT(stack, "b"));
		param.add(stack);
		param.add(stack);
		assertTrue(b.getMethodes().chercherOptimum("f1", param).equals(af1));
	}

}
