package mjc.tds;

import java.util.List;

import mjc.type.ARGUMENT;
import mjc.type.DTYPE;
import mjc.type.PARAMETRES;

public class INFOPROCEDURE extends INFOMETHODE {

	public INFOPROCEDURE(String nom, PARAMETRES arguments) {
		super(new DTYPE("void",0), nom, arguments);
	}

}
