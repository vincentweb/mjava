package mjc.compiler;

import java.io.Serializable;
import mg.egg.eggc.compiler.libjava.ISourceUnit;

public class MJC implements Serializable {
	private static final long serialVersionUID = 1L;

	public static void main(String[] args) {
		MJ1 mj1 = new MJ1();
		ISourceUnit cu = mj1.compile(args);
		MJ2 mj2 = new MJ2();
		ISourceUnit cu2 = mj2.compile(cu);
		MJ3 mj3 = new MJ3();
		mj3.compile(cu2);
	}
}
