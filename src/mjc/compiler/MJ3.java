package mjc.compiler;

import mg.egg.eggc.compiler.libjava.ISourceUnit;
import mg.egg.eggc.compiler.libjava.problem.IProblem;
import mg.egg.eggc.compiler.libjava.problem.ProblemReporter;
import mg.egg.eggc.compiler.libjava.problem.ProblemRequestor;
import mjc.egg.MJAVA3;

public class MJ3 {
	public void compile(ISourceUnit cu){
		System.err.println("version " + "0.0.1");
		try {
			ProblemReporter prp = new ProblemReporter(cu);
			ProblemRequestor prq = new ProblemRequestor();
			MJAVA3 compilo = new MJAVA3(prp);
			prq.beginReporting();
			compilo.set_source((MJAVASourceFile) cu);
			compilo.compile(cu);
			for (IProblem problem : prp.getAllProblems())
				prq.acceptProblem(problem);
			prq.endReporting();
			System.err.println(Messages.getString("MJC.ok")); //$NON-NLS-1$
			System.exit(0);
		} catch (MJCException e) {
			System.err.println(e.getMessage());
			System.exit(1);
		} catch (Exception e) {
			e.printStackTrace();
			System.exit(1);
		}
	}
}
