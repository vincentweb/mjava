package mjc.gc;

import java.util.ArrayList;

import mjc.tds.INFO;
import mjc.tds.INFOMETHODE;
import mjc.tds.INFOTYPE;
import mjc.tds.TDS;
import mjc.type.ARGUMENT;
import mjc.type.CLASSE;
import mjc.type.DTYPE;
import mjc.type.INTERFACE;
import mjc.type.PARAMETRES;

public class Utilitaires_MTest {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		TDS<INFO> tds = new TDS<INFO>();
		TDS<INFO> tdsf = new TDS<INFO>(tds);
		
		
		DTYPE integer = new DTYPE("int",1);
		//DTYPE bool = new DTYPE("boolean",1);
		DTYPE vide = new DTYPE("void",0);
		
		String nom0 = "Nananere";
		INTERFACE interf = new INTERFACE(nom0);
		INFOTYPE info0 = new INFOTYPE(interf);
		
		String nom1 = "Point";
		CLASSE classe1 = new CLASSE(nom1);
		classe1.addInterface(interf);
		INFOMETHODE methode1 = new INFOMETHODE(integer, "m1", new PARAMETRES());
		classe1.addMethode(methode1);
		INFOTYPE info1 = new INFOTYPE(classe1);
		
		String nom2 = "PointNomme";
		CLASSE classe2 = new CLASSE(nom2);
		classe2.addParent(classe1);
		INFOMETHODE methode2 = new INFOMETHODE(vide, "m2", new PARAMETRES());
		classe2.addMethode(methode1);
		classe2.addMethode(methode2);
		INFOTYPE info2 = new INFOTYPE(classe2);
		
				
		tds.inserer(nom0, info0);
		tds.inserer(nom1, info1);
		tds.inserer(nom2, info2);
		
//		System.out.println(classe2);
		
		mjc.gc.Utilitaires.genererNumerosMethodes(tdsf);
		//mjc.gc.Utilitaires.genererNumerosClasses(tdsf);
		
		INFO res1 = tds.get("Point");
		CLASSE resC1 = (CLASSE) res1.getType();
		System.out.println(resC1.getNumerosMethodes());
		System.out.println("-----" + resC1.getNumeroClasse());
		
		INFO res2 = tds.get("PointNomme");
		CLASSE resC2 = (CLASSE) res2.getType();
		System.out.println(resC2.getNumerosMethodes());
		System.out.println("-----" + resC2.getNumeroClasse());
		
		
	}
}
