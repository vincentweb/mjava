package mjc.gc;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Set;

import mjc.type.*;
import mjc.tds.*;

public class TAM extends AbstractMachine {

	public String getSuffixe() {
		return "tam";
	}
	
	private String nom;

	public TAM(String fname) {
		if (fname.endsWith(".tam")) {
			nom = fname.substring(0, fname.length() - 5);
		} else {
			nom = fname;
		}
	}
	
	public TAM() {
		nom = "toto.tam";
	}

	public String genFonction(String etiquette, int taillepars, int tailleretour,
			String code) {
		return "_" + etiquette + "\n" + code + "\tRETURN (" + taillepars + ") "
				+ tailleretour + "\n";

	}

	public String genCall(String etiquette, String code) {
		return "; Appel a " + etiquette + "\n" + code + "\tCALL(SB) _" + etiquette + "\n";
	}

	// genere le code pour une declaration (avec initialisation)
	public String genDecl(String n, INFOVAR i, String t) {
		int taille = i.getType().getTaille();
		if (t.equals("")) {
			return "\tPUSH "+taille+"\n";
		}
		else {
			return "   ; decl de " + n + " en " + i.getDep() + "/" + i.getReg()
					+ " taille = " + taille + "\n" + t + "\n"
					//+ genMem(i.getDep(), "LB", taille) + "\n"
					/*+ genReadMem(taille)*/;
		}
	}
	

	// compteur pour le generateur d'etiquettes
	private static int cpt = 0;

	// genere une etiquette differente des autres
	public String genEtiq() {
		return "X" + cpt++;
	}

	// genere le code pour l'arret de la machine
	public String genFin() {
		return "\tHALT\n";
	}

	public void genAsm(String code, String appel) {
		try {
			PrintWriter pw = new PrintWriter(new FileOutputStream(nom + ".tam"));
			pw.println(";;; code TAM engendre pour " + nom + "\n");
			pw.print(appel);
			pw.print(genFin());
			pw.print(code);
			pw.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}

	public String genCst(String v) {
		return "\tLOADL " + v + "\n";
	}

	public String genFree(int i) {
		// TODO Auto-generated method stub
		return "\tPOP (0) " + i + "\n";
	}

	public String genMem(int dep, String reg, int taille) {
		// TODO Auto-generated method stub
		return "\tLOAD(" + taille + ") " + dep + "[" + reg + "]\n";
	}

	public String genStore(int dep, String reg, int taille) {
		// TODO Auto-generated method stub
		return "\tSTORE(" + taille + ") " + dep + "[" + reg + "]\n";
	}
	
	public String genReadMem(int taille) {
		// TODO Auto-generated method stub
		return "\tLOADI(" + taille + ")\n";
	}

	public String genWriteMem(int taille) {
		// TODO Auto-generated method stub
		return "\tSTOREI(" + taille + ")\n";
	}

	public String genIf(String code, String code2, String code3) {
		String sinon = genEtiq();
		String res = "\t; if\n" + code + "\tJUMPIF(0) " + sinon
						+ "\n" + code2 ;
		if (code3.equals("")) {
			res += sinon + "\n" ;
		} else {
			String fin = genEtiq();
			res += "\tJUMP " + fin + "\n" + sinon + "\n" + code3 + 
				fin + "\n" + "\t; fin if\n";
		}
		return res ;
	}

	public String genMalloc(int taille) {
		return "\tLOADL " + taille + "\n" + "\tSUBR Malloc\n";
	}

	public String genAdr(int dep, String reg) {
		return "\tLOADA " + dep + "[" + reg + "]\n";
	}

	public String genAdrField(int dep) {
		return "\tLOADL " + dep + "\n\tSUBR Iadd\n";
	}

	public String genComment(String c) {
		return "; " + c + "\n";
	}
	
	@SuppressWarnings("unchecked")
	public String genVTable(TDS<INFO> tds_) {
		StringBuilder result = new StringBuilder();
		TDS<INFO> tds = tds_;
		/*On remonte à la TDS mère au cas où*/
		while(tds.getParente() != null) {
			tds = tds.getParente();
		}
		
		
		ArrayList<CLASSE> liste = Utilitaires.getRankedList(tds);
//		for(CLASSE classe : liste) {
//			System.out.println("OOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO");
//			System.out.println(classe.getNom());
//			System.out.println("OOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO");
//		}
		
		
//		Utilitaires.genererNumerosMethodes(tds);
	
		Set<String> classes = ((HashMap<String, INFO>) tds.clone()).keySet();
		String element;
		INFO info;
		CLASSE classe;
		int nbMethodes;
		int nbMethodesTot = 0;
		
		int noClasse = 0;
		result.append("\tPUSH 1 ; 1e emplacement = addr VTable\n");
		
		result.append(";#################################\n");
		result.append("; Generation de la VTable \n");
		result.append(";#################################\n");
		
		
		for(CLASSE c : liste) {
			result.append(";# CLASSE : " + c.getNom() + "\n");
			nbMethodes = c.getNumerosMethodes().size();
			nbMethodesTot += nbMethodes;
			int adresseCourante = 0;
			int adresse;
//			System.out.println("##### "+classe.getNumerosMethodes());
			for(INFOMETHODE methode : c.getNumerosMethodes()) {
				adresse = adresseCourante + methode.getTaille();
				result.append("; "+ methode.getClasse().getNom() + " - " + methode.getNom() + "\n");
				//result.append("\tLOADL " + adresse + "\n");
				result.append("\tLOADA " + methode.getLabel() + "\n");
				adresseCourante = adresse;
			}
			/* *********************** */
	
			noClasse += nbMethodes;

		}
		
		
//		while((element = Utilitaires.getOneSetElement(classes)) != null) {
//			
////			System.out.println(element);
//			
//			
//			
//			result.append(";# CLASSE : " + element + "\n");
//			
//			info = tds.get(element);
//			if(info.getType() instanceof CLASSE) {
//				classe = (CLASSE) info.getType();
//				nbMethodes = classe.getNumerosMethodes().size();
//				nbMethodesTot += nbMethodes;
////				classe.setNumeroClasse(noClasse);
//				/* *********************** */
//				/* Generation du code      */
//				
//				//result.append("DEBUG "+ classe.getNom() + "\n");
////				result.append("DEBUG "+ classe.getNumerosMethodes() + "\n");
//				
//				int adresseCourante = 0;
//				int adresse;
////				System.out.println("##### "+classe.getNumerosMethodes());
//				for(INFOMETHODE methode : classe.getNumerosMethodes()) {
//					adresse = adresseCourante + methode.getTaille();
//					result.append("; "+ methode.getClasse().getNom() + " - " + methode.getNom() + "\n");
//					//result.append("\tLOADL " + adresse + "\n");
//					result.append("\tLOADA " + methode.getLabel() + "\n");
//					adresseCourante = adresse;
//				}
//				/* *********************** */
//		
//				noClasse += nbMethodes;
//				classes.remove(classe.getNom());
//			} else { // C'est certainement une interface ....
//				classes.remove(element);
//			}
//		}
		
		/* *********************** */
		/* Generation du code      */
		
		result.append("\tLOADL " + nbMethodesTot + "\n");
		result.append("\tSUBR MAlloc \n");
		result.append("\tSTOREI (" + nbMethodesTot +")\n");
		result.append(";#################################\n");
		result.append(";Fin de la generation de la VTable\n");
		result.append(";#################################\n");
		
		result.append("\tLOADA 1[HT]\n"); // là où commence la VTable
		result.append("\tPOP (1) 1\n"); // qu'on enregistre en 0[SB]
		
		/* *********************** */
		
		return result.toString();
	}
	
	
	public String genMain(INFOMETHODE main) {
		
		
		// Génération de code
		
		StringBuilder result = new StringBuilder();
		result.append(";#############################\n");
		result.append("; Appel du main\n");
		result.append(";#############################\n");
		result.append(this.genNew(main.getClasse(), main, "")); // Dans la pratique il faudrait appeler le constructeur puis le main, mais la classe qui contient le main n'a pas forcément de constructer :'( TODO: à discuter 
		//result.append("\tLOADL " + args + "\n"); // --> plus d'arguments autorisés
		//result.append("\tCALL (LB) " + main.getLabel() + "\n");
		result.append("\tPOP (0) 1\n"); // on supprime O[SB] --> Attention car si il y a des arguments definis dans main, le return va supprimer une case de trop et le pop va nous envoyer dans l'hyper espace !!
		result.append("\tHALT\n");
		result.append(";#############################\n");
		result.append(";Fin Appel du main\n");
		result.append(";#############################\n");
		
		return result.toString();
	}
	
	
	public String genOpUn(String op, String code) {
		if (op.equals(""))
			return code;
		else if (op.equals("INeg")) {// bugguée
			return "\tLOADL 0\n"+code + "\tSUBR ISub\n";
		}
		return  code + "\tSUBR "+op+"\n";
	}
	
	
	public String genOpBin(String op, String code, String code2) {
		if (op.equals(""))
				return code+"\n"+code2;
		return  code + "\tSUBR "+op+"\n" + code2;
		
	}
	
	public String genSeq(String code1, String code2) {
		if (code1==null)
			code1 = "";
		if (code2==null)
			code2 = "";
		return code1  + code2;
	}
	
	public String genCall(INFOMETHODE methode, String arguments) {
		int argumentSize = methode.getArguments().getTaille();
		return "\t; ************ APPEL de "+methode.getNom()+"\n"
			 + arguments                                              // arguments
			 //+ "\tLOAD (1) -"+String.valueOf(methode.getArguments().getTaille()+1)+"[ST]\n"
			 + genCst("0")                                            // pour le CALLI
			 + "\tLOAD (1) "+String.valueOf(-2)+"[ST]\n" // récupérer le pointeur de l'objet courant qui est en ST-2-argumentSize  (on l'a en double, on supprime le doublon après le RETURN)
			 + genReadMem(1)                                          // récupérer la 1e case de l'instance de l'objet = numéro de la classe
			 + genCst(String.valueOf(methode.getNumero()))            // récupére le numéro de la méthode
			 + "\tSUBR IAdd\n"                                        // numClasse + numMethode = position dans la VTable
			 + genMem(0, "SB", 1)                                     // on récupère l'adresse où commence la VTable
			 + "\tSUBR IAdd\n"                                        // on a l'adresse du tas où chercher l'adresse de la fonctions
			 + genReadMem(1)                                          // on a l'adresse de la fonction
			 + "\tCALLI\n"; 
			 //+ "\tPOP (0) 1\n";                                       // pointeur de l'objet courant en doublon qu'on supprime
	}

	public String genReturn(String code, int tailleParam, int tailleRetour) {
        return "; RETURN -- \n"
            + code 
            +"\tRETURN " + "(" + tailleRetour + ") " + tailleParam +"\n";
    }
	
	public String genEtiquette(CLASSE classe, INFOMETHODE infomethode) {
		return classe.getNumeroClasse() + "_" + infomethode.getNumero() + "\n";
	}
	
	public String genMethode(CLASSE classe, INFOMETHODE infomethode, String corps) {
		return "; -------------> "+classe.getNom()+" - "+infomethode.getNom()+"\n"+infomethode.getLabel() + corps;
	}
	
	// A la fin, il doit rester l'adresse renvoyée par le malloc, je ne sais pas si c'est le cas
	// Je pense qu'on devarit avoir un CLASSE.getConstructeur(Arguments)
	public String genNew(CLASSE classe, INFOMETHODE methode, String arguments) {
		return "\t ;********* New " + classe.getNom() + "\n"
				//+ arguments // genCall s'en occuppe
				+ "\tLOADL " + methode.getClasse().getSize() + "\n"
				+ "\tSUBR MAlloc \n"   // allocation, on récupère l'adresse du pointeur
				+ "\tLOADL "+ classe.getNumeroClasse() + "\n"
				+ "\tLOAD (1) -2[ST]\n"  // on récupère l'adresse renvoyée par MAlloc
				+ "\tSTOREI (1)\n"
				// empiler les arguments, on le faire faire par genCall
				+ genCall(methode, arguments);
	
	}
	
	public String genAttr(INFOATTRIBUT info) {
		return  "\tLOADL " + info.getDeplacement() +"\n"
			    +"\tSUBR IAdd\n";
	}
	
	public String genCommentaire(String string) {
		return "; " + string + "\n";
	}
	
	public String genPrintI() {
		return "\tSUBR IOut\n"+"\tLOADL \"\n\"\n\tSUBR SOut\n"; 
	}
	
}
