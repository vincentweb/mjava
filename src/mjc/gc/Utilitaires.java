package mjc.gc;

import java.util.AbstractSet;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Set;

import mjc.tds.INFO;
import mjc.tds.INFOMETHODE;
import mjc.tds.TDMETH;
import mjc.tds.TDS;
import mjc.type.CLASSE;
import mjc.type.DTYPE;
import mjc.type.INTERFACE;

public class Utilitaires {
	
	@SuppressWarnings("unchecked")
	public static void genererNumerosMethodes(TDS<INFO> tds_) {
		TDS<INFO> tds = tds_;
		/*On remonte à la TDS mère au cas où*/
		while(tds.getParente() != null) {
			tds = tds.getParente();
		}
		
	
		Set<String> classes = ((HashMap<String, INFO>) tds.clone()).keySet();
//		System.out.println("init i0>>"+classes.contains("I0"));
//		System.out.println("init i>>"+classes.contains("I"));
		String element;
		INFO info;
		CLASSE classe;
		INTERFACE interf;
		ArrayList<INFOMETHODE> numeros;
		TDMETH methodes;
		int numMethodes = 0;
		
		while((element = getOneSetElement(classes)) != null) {
			info = tds.get(element);
			if(info.getType() instanceof CLASSE) {
				classe = (CLASSE) info.getType();
				/*On remonte à la classe mère ultime que l'on a pas deja visité*/
				while(classe.getParent() != null && classes.contains(classe.getParent().getNom())) {
					classe = classe.getParent();
				}
				
				if(classe.getInterface() == null || (classe.getInterface() != null && !classes.contains(classe.getInterface().getNom()))) {
				
					if(classe.getParent() != null) {
						numeros = (ArrayList<INFOMETHODE>) classe.getParent().getNumerosMethodes().clone();
					} 
					else if(classe.getInterface() != null) {
						numeros = (ArrayList<INFOMETHODE>) classe.getInterface().getNumerosMethodes().clone();
						for(INFOMETHODE infometh : numeros) {
							infometh.setClasse(classe);
						}
					}
					else {
						numeros = new ArrayList<INFOMETHODE>();
					}
					
					methodes = classe.getMethodes();
					
					
					if(numeros.size() != 0) {
					// 	Pas une classe mère ultime
						for(INFOMETHODE methode : methodes) {
							boolean present = false;
							for(int i=0; i<numeros.size(); i++) {
								if(methode.equals(numeros.get(i))) {
									present = true;
									numeros.set(i, methode);
								}
							}
							if(!present) {
								numeros.add(methode);
							}
							present = false;
						}
					} 
					else {
					// 	classe mère ultime
						numeros.addAll(methodes);
					}
					// On met l'ArrayList dans la classe qui va bien
					classe.setNumerosMethodes(numeros);
					classe.setNumeroClasse(numMethodes);
					numMethodes += numeros.size();
					
					// Et on supprime de la liste des classes celle sur laquelle on a travaillé
					classes.remove(classe.getNom());
//					System.out.println("remove "+classe.getNom());
					
	//				System.out.println("////////// NUMEROS " + classe.getNom() +" = " + numeros);
					
//					System.out.println("####################"+classe.getNom()+"##################");
					// On met à jour les numéros de méthode dans les INFOMETHODES de la classe
					for (int i=0; i<numeros.size(); i++) {
						numeros.get(i).setNumero(i);
//						System.out.println(i + " : " + numeros.get(i).getNom());
	
					}

				} 
				else {
					interf = classe.getInterface();
//					System.out.println("I>>"+classes.contains(interf.getParent().getNom()));
//					System.out.println("I>>"+classes.contains("A"));
					while(interf.getParent() != null && classes.contains(interf.getParent().getNom())) {
						interf = interf.getParent();
					}
					
//					System.out.println(classes);
					
					if(interf.getParent() != null) {
						numeros = (ArrayList<INFOMETHODE>) interf.getParent().getNumerosMethodes().clone();
					} else {
						numeros = new ArrayList<INFOMETHODE>();
					}
					
					methodes = interf.getMethodes();
					
				
					if(numeros.size() != 0) {
						// Pas une classe mère ultime
						for(INFOMETHODE methode : methodes) {
							boolean present = false;
							for(int i=0; i<numeros.size(); i++) {
								if(methode.equals(numeros.get(i))) {
									present = true;
									numeros.set(i, methode);
								}
							}
							if(!present) {
								numeros.add(methode);
							}
							present = false;
						}
					} 
					else {
						// classe mère ultime
						numeros.addAll(methodes);
					}
					// On met l'ArrayList dans la classe qui va bien
					interf.setNumerosMethodes(numeros);
//					numMethodes += numeros.size();
					
					// Et on supprime de la liste des classes celle sur laquelle on a travaillé
					classes.remove(interf.getNom());
					System.out.println("remove "+interf.getNom());
					
//					System.out.println("////////// NUMEROS " + interf.getNom() +" = " + numeros);
					
//					System.out.println("####################"+interf.getNom()+"##################");
					// On met à jour les numéros de méthode dans les INFOMETHODES de la classe
					for (int i=0; i<numeros.size(); i++) {
						numeros.get(i).setNumero(i);
//						System.out.println(i + " : " + numeros.get(i).getNom());
					}
					
					
				}
				
				
//			} else if (info.getType() instanceof INTERFACE) {
//				interf = (INTERFACE) info.getType();
//				/*On remonte à la classe mère ultime que l'on a pas deja visité*/
//				while(interf.getParent() != null && classes.contains(interf.getParent().getNom())) {
//					interf = interf.getParent();
//				}
//				
//				
				

			} else {
				interf = (INTERFACE) tds.get(element).getType();
				while(interf.getParent() != null && classes.contains(interf.getParent().getNom())) {
					interf = interf.getParent();
				}
				
//				System.out.println(classes);
				
				if(interf.getParent() != null) {
					numeros = (ArrayList<INFOMETHODE>) interf.getParent().getNumerosMethodes().clone();
				} else {
					numeros = new ArrayList<INFOMETHODE>();
				}
				
				methodes = interf.getMethodes();
				
			
				if(numeros.size() != 0) {
					// Pas une classe mère ultime
					for(INFOMETHODE methode : methodes) {
						boolean present = false;
						for(int i=0; i<numeros.size(); i++) {
							if(methode.equals(numeros.get(i))) {
								present = true;
								numeros.set(i, methode);
							}
						}
						if(!present) {
							numeros.add(methode);
						}
						present = false;
					}
				} 
				else {
					// classe mère ultime
					numeros.addAll(methodes);
				}
				// On met l'ArrayList dans la classe qui va bien
				interf.setNumerosMethodes(numeros);
//				numMethodes += numeros.size();
				
				// Et on supprime de la liste des classes celle sur laquelle on a travaillé
				classes.remove(interf.getNom());
//				System.out.println("remove "+interf.getNom());
				
//				System.out.println("////////// NUMEROS " + interf.getNom() +" = " + numeros);
				
//				System.out.println("####################"+interf.getNom()+"##################");
				// On met à jour les numéros de méthode dans les INFOMETHODES de la classe
				for (int i=0; i<numeros.size(); i++) {
					numeros.get(i).setNumero(i);
//					System.out.println(i + " : " + numeros.get(i).getNom());
				}
			}
		}
	}
	
	
	@SuppressWarnings("unchecked")
	public static ArrayList<CLASSE> getRankedList(TDS<INFO> tds_) {
		TDS<INFO> tds = tds_;
		ArrayList<CLASSE> result = new ArrayList<CLASSE>();
		/*On remonte à la TDS mère au cas où*/
		while(tds.getParente() != null) {
			tds = tds.getParente();
		}
		
		
		Set<String> classes = ((HashMap<String, INFO>) tds.clone()).keySet();
		
		ArrayList<String> toRemove = new ArrayList<String>();
		Iterator<String> iter = classes.iterator();
		while(iter.hasNext()) {
			String element = iter.next();
			if(tds_.get(element).getType() instanceof INTERFACE) {
				toRemove.add(element);
			}
		}
		for (String element : toRemove) {
			classes.remove(element);
		}
//		System.out.println("DEBUG "+classes);
//		System.out.println("########################################################################################");
		
		
		
		while(!classes.isEmpty()) {
			Object[] classesTab = classes.toArray();
			//System.out.println(classes);
			
			CLASSE c = null;
			int j = -1;
			int numeroClasse = 1000000;
			DTYPE type;
			
			for(int i = 0; i<classes.size(); i++) {
				type = tds.get(classesTab[i]).getType();
				if (type instanceof CLASSE) {
//					System.out.println(((CLASSE) type).getNumeroClasse());
//					System.out.println(numeroClasse);
//					System.out.println("-----------------------------");
					if (((CLASSE) type).getNumeroClasse() <= numeroClasse) {
//						System.out.println(type.getNom());
						numeroClasse = ((CLASSE) type).getNumeroClasse();
						c = (CLASSE) type;
						j=i;
					}
				} else {
					classes.remove(classesTab[i]);
				}
			}
			
//			System.out.println(j);
			
			if(c!=null) {
				result.add(c);
			}
			
//			System.out.println(classes);
//			System.out.println(classesTab[j]);
			try {
				classes.remove(classesTab[j]);
			}
			catch(Exception e) {
				System.out.println("Cannot remove "+j);
			}
			
		}
//		System.out.println("DEBUG "+result);
		return result;
		
	}
	
	
	public static String getOneSetElement(Set<String> set) {
		if(set.isEmpty()) {
			return null;
		} else {
			Object[] t = set.toArray();
			return t[0].toString();
		}
	}
	
	
//	//commence à 0 puis ajoute le nombre de methodes pour avoir la classe d'après, etc ... N'est pas utilisé, copié dans TAM.java
//	@SuppressWarnings("unchecked")
//	public static void genererNumerosClasses(TDS<INFO> tds_) {
//		TDS<INFO> tds = tds_;
//		/*On remonte à la TDS mère au cas où*/
//		while(tds.getParente() != null) {
//			tds = tds.getParente();
//		}
//		
//	
//		Set<String> classes = ((HashMap<String, INFO>) tds.clone()).keySet();
//			
//		String element;
//		INFO info;
//		CLASSE classe;
//		int nbMethodes;
//		
//		int noClasse = 0;
//		
//		while((element = getOneSetElement(classes)) != null) {
//			
////			System.out.println(element);
//			
//			info = tds.get(element);
//			if(info.getType() instanceof CLASSE) {
//				classe = (CLASSE) info.getType();
//				nbMethodes = classe.getNumerosMethodes().size();
//				classe.setNumeroClasse(noClasse);
//				noClasse += nbMethodes;
//				classes.remove(classe.getNom());
//			} else { // C'est certainement une interface ....
//				classes.remove(element);
//			}
//		}
//	}
	
}
