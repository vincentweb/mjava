package mjc.type;

public enum OP_REL {
	INF,
	INFEG,
	SUP,
	SUPEG,
	EG,
	NEG
}
