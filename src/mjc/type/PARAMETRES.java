package mjc.type;

public class PARAMETRES extends ArrayListe<ARGUMENT> {

	private static final long serialVersionUID = -376422606393970165L;
	private int taille;

	public PARAMETRES(){
		super();
		taille = 0;
	}
	
	public boolean estAjoutable(ARGUMENT param){
		for (ARGUMENT a : this){
			if ( a.getNom().equals(param.getNom()) )
				return false;
		}
		return true;
	}
	
	public void inserer(ARGUMENT argument){
		this.add(argument);
		taille += argument.getType().getTaille();
	}
	
	public int getTaille() {
		return taille;
	}
	
}
