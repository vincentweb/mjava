package mjc.type;

public class CLASSE_MTest {
	
	public static void main(String[] args) {
		CLASSE a = new CLASSE("a");
		CLASSE b = new CLASSE("b");
		b.addParent(a);
		CLASSE c = new CLASSE("c");
		
		/*ok a de l'héritage linéaire, cool*/
		System.out.println(c.extendsRecursifs(b));
		
		CLASSE d = new CLASSE("d");
		CLASSE e = new CLASSE("e");
		CLASSE f = new CLASSE("f");
		d.addParent(f);
		e.addParent(d);
		
		/*On a de la circularité*/
		System.out.println(f.extendsRecursifs(e));
	}
	
}