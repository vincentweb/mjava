package mjc.type;

import java.util.List;

import mjc.tds.INFOATTRIBUT;
import mjc.tds.INFOMETHODE;
import mjc.tds.TDMETH;

// Pour le nom, j'hésitais avec moule, coquille et shell
public interface STRUCTURE {
	void addAttribut(String nom, INFOATTRIBUT attribut);
	void addMethode(INFOMETHODE methode);
	INFOMETHODE rechercherMethode(String nom, List<ARGUMENT> arguments);
	INFOATTRIBUT chercherGlobalementAttribut(String nom);
	INFOMETHODE chercherOptimumMethode (String nom, List<DTYPE> arguments);
	String getNom();
    int distanceDe(DTYPE autre);
}
