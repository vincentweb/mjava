package mjc.type;

public class DTYPE {
	
	protected int taille;
	
	public int getTaille() {
	        return taille;
	}
	
	protected String nom;
	
	public String getNom() {
	        return nom;
	}
	
	public DTYPE(String n, int t) {
	        nom = n;
	        taille = t;
	}
	
	public boolean compareTo(DTYPE autre) {
	        return nom.equals(autre.nom);
	}
	
	@Override
	public boolean equals(Object type){
		if(type instanceof DTYPE) {
		    return this.nom.equals(((DTYPE)type).getNom());
		}
		else {
			return false;
		}
    }
	
	
	public String toString() {
			if (nom != null)
				return "DTYPE : "+nom + "(" + taille + ")";
			else
				return "!! DTYPE : pas printable";
	}
	
	public int distanceDe(DTYPE autre) {
		if (this.compareTo(autre))
			return 0;
		else
			return -1;
    }
}
