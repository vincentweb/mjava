package mjc.type;

import java.util.ArrayList;
import java.util.List;

import mjc.tds.INFOATTRIBUT;
import mjc.tds.INFOMETHODE;
import mjc.tds.TDMETH;
import mjc.tds.TDS;

public class INTERFACE extends DTYPE implements STRUCTURE{

	private ArrayList<INFOMETHODE> numerosMethodes;
    private TDMETH methodes;
    private String nom;
    private INTERFACE interfParente;
	
    public INTERFACE(String nom) {
		super(nom, 1);
		this.nom = nom;
		interfParente = null;
		methodes = new TDMETH();
	}
    
    public void addInterfParente(INTERFACE interfParente){
    	this.interfParente = interfParente;
    	methodes = new TDMETH(interfParente.methodes);
    	
    	// vérifier que this != interfParente && this interfParente.interfParente && ... pour éviter les récursions cycliques
    	// en faite, ce cas ne peut se produire que si on autorise à utiliser une classe avant de l'avoir déclarée
    	INTERFACE i = interfParente;
    	while (i.getParent()!=null){
    		if (this.nom.equals(i.getNom())){
    			// Il faut mieux que ça soit EGG qui arrête la compilation par un error() ?
    			// le RuntimeException m'épargne de mettre un trowable
    			throw new RuntimeException("Extend cyclique détecté !");
    		}
    		i = i.getParent();
    	} // On prie pour que le garbageCollector détruise "i" une fois arrivé ici.
    }
    
    public String getNom(){
    	return nom;
    }
    
    public INTERFACE getParent(){
    	return interfParente;
    }
    
    public boolean compareTo(DTYPE autre) {
    	if (!(autre instanceof STRUCTURE)){
    		return false;
    	}
    	if (nom==autre.getNom())
    		return true;
    	else if (interfParente == null){
    		return false;
    	}
    	else {
    		return interfParente.compareTo(autre);
    	}
    }

    public TDMETH getMethodes() {
    	return this.methodes;
    }

	@Override
	public void addAttribut(String nom, INFOATTRIBUT attribut) {
		throw new RuntimeException();
	}

	@Override
	public void addMethode(INFOMETHODE methode) {
		methodes.inserer(methode, null);
		
	}
	
	@Override
	public INFOMETHODE rechercherMethode(String nom, List<ARGUMENT> arguments) {
		return methodes.chercherLocalement(nom, arguments);
	}
	
	@Override
	public INFOMETHODE chercherOptimumMethode (String nom, List<DTYPE> arguments) {
	    try {
		return methodes.chercherOptimum(nom, arguments);
	    }
	    catch (Exception e) {
		return null;
	    }
	}
	
	@Override
	public INFOATTRIBUT chercherGlobalementAttribut(String nom){
		throw new RuntimeException();
	}
	
	@Override
	public String toString(){
//		return "interface";
		return nom;
	}

    public int distanceDe(DTYPE autre) {
    	if (!(autre instanceof STRUCTURE))
    		return -1;
		if(nom.equals(autre.getNom())) {
		    return 0;
		} 
		else {
		    if(interfParente == null) {
		    	return -1;
		    } 
		    else {
				int recursion = interfParente.distanceDe(autre);
				return recursion<0 ? -1 : 1+recursion;
		    }
		}
    }

    public boolean implementsRecursifs(INTERFACE inter) {
    	while(inter != null) {
    		if(inter.getNom().equals(this.nom)){
    			return true;
    		} else {
    			inter = inter.getParent();
    		}
    	}
    	return false;
    }

	public ArrayList<INFOMETHODE> getNumerosMethodes() {
		return numerosMethodes;
	}

	public void setNumerosMethodes(ArrayList<INFOMETHODE> numerosMethodes) {
		this.numerosMethodes = numerosMethodes;
	}

}
