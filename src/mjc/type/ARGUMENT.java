package mjc.type;

public class ARGUMENT {
    private DTYPE type;
    private String nom;
    
    public ARGUMENT(DTYPE type, String nom){
		this.type = type;
		this.nom = nom;
    }
    
    public DTYPE getType(){
    	return type;
    }
    
    public String getNom(){
    	return nom;
    }
    
    @Override
	public boolean equals(Object argument){
		if(argument instanceof ARGUMENT) {
		    return type.equals(((ARGUMENT)argument).getType()); // deux arguments sont égaux si ils ont le même type.
		} 
		else 
			return false;
    }
    
    @Override
	public int hashCode() {
    	return nom.hashCode(); // inutile mais utile pour java
    }
    
    public boolean compareTo(ARGUMENT autre) {
    	return this.type.compareTo(autre.getType());
    }
    
    public String toString(){
    		return "Argument "+nom+" : "+type.getNom() + " ("+type.getClass()+")";
    }
}
