package mjc.type;


import java.util.ArrayList;
import java.util.List;

import mjc.tds.INFOATTRIBUT;
import mjc.tds.INFOMETHODE;
import mjc.tds.TDMETH;
import mjc.tds.TDS;


// C'est délicat au 1e abord de dire si ça doit hériter de DTYPE on de INFO
//  une classe est un type, en TP STRUCT hérite de DTYPE, pour ranger les CLASSES dans la TDS on met les classes/structs dans des INFOTYPE 

public class CLASSE extends DTYPE implements STRUCTURE {
	
	private ArrayList<INFOMETHODE> numerosMethodes;
    private String nom;
    private TDMETH methodes;
    private TDS<INFOATTRIBUT> attributs;
    private CLASSE classeParente;
    private INTERFACE interf; // interface est un mot-clef Java :(
	private int noClasse;
	private int size; // taille de la classe = 1 + sum(attributs.size())
	// int taille = taille du pointeur
    
    public CLASSE(String nom) {
		super(nom, 1);
		this.nom = nom;
		this.interf = null;
		this.classeParente = null;
		this.attributs = new TDS<INFOATTRIBUT>();
		this.methodes = new TDMETH();
		noClasse = 1337;
		this.size = 1; // Initialement on prend une case dans le tas = noClasse
	}
    
    public CLASSE(STRUCTURE struct){
    	this(((CLASSE)struct).getNom());
    	if (struct instanceof CLASSE){
    		this.interf = ((CLASSE)struct).getInterface();
    		this.classeParente = ((CLASSE)struct).getParent();
    		this.attributs = ((CLASSE)struct).getAttributs();
    		this.methodes = ((CLASSE)struct).getMethodes();
    		this.noClasse = ((CLASSE)struct).getNumeroClasse();
    		this.taille = ((CLASSE)struct).getTaille();
    		this.size = ((CLASSE)struct).getSize();
    	}
    }
    
    public void addParent(CLASSE classeParente) {
    	this.classeParente = classeParente;
    	methodes = new TDMETH((TDMETH)classeParente.methodes.clone());
    	attributs = new TDS<INFOATTRIBUT>(classeParente.attributs);
    	this.size += classeParente.getSize();
    	
    	// vérifier que this != interfParente && this interfParente.interfParente && ... pour éviter les récursions cycliques
    	// en faite, ce cas ne peut se produire que si on autorise à utiliser une classe avant de l'avoir déclarée
    	CLASSE p = classeParente;
    	while (p.classeParente!=null){
    		if (this.nom.equals(p.getNom())){
    			// Il faut mieux que ça soit EGG qui arrête la compilation par un error() ?
    			// le RuntimeException m'épargne de mettre un trowable
    			throw new RuntimeException("Extend cyclique détecté !");
    		}
    		p = p.classeParente;
    	} // On prie pour que le garbageCollector détruise "i" une fois arrivé ici.
    }
    
    /**
     * 
     * @param interf
     */
    public void addInterface(INTERFACE interf){
    	this.interf = interf;
    }
    
    public boolean interfaceRespectee(){
    	return interfaceRespectee(interf);
    }
    
    public CLASSE getParent(){
    	return this.classeParente;
    }
    
    public INTERFACE getInterface(){
    	return this.interf;
    }
    
//    public int getSize() {
//    	return size;
//    }
    
    // Renvoyer aussi un message d'erreur pour l'utilisateur ?
    public boolean interfaceRespectee(INTERFACE interf){
    	if (interf == null){
    		return true;
    	}
    	// On prend toutes les méthodes de l'interface, et on regarde si elles ont été définies dans notre classe, ou dans la classe parente
    	for (INFOMETHODE imeth : interf.getMethodes()) {
    		// rechercher si dans la classe, il y a une méthode “meth” tq meth.getNom() == imeth.getNom()
			// et tq meth.getTypeRetour() ≤ imeth.getTypeRetour() && meth.getArgs() == imeth.getArgs()
    		if (this.methodes.chercherGlobalementInterf(imeth) == null)
    			return false;
    	}
    	// on vérifie si les interfaces parentes ne sont implémentées
    	if (interf.getParent() != null)
    		return interfaceRespectee(interf.getParent());
    	// On arrive là que si tout s'est bien passé
    	return true;
    }


    @Override
    public INFOATTRIBUT chercherGlobalementAttribut(String nom){
    	return attributs.chercherGlobalement(nom); 
    }
    
    @Override
    public String getNom(){
    	return nom;
    }
    
    // classeFille.compareTo(classeMere) ==> true
    public boolean compareTo(DTYPE autre) {
    	if (!(autre instanceof STRUCTURE)){
    		return false;
    	}
    	if (nom==autre.getNom())
    		return true;
    	else if ( (classeParente == null) && (interf == null) ){
    		return false;
    	}
    	else {
    		return classeParente != null && classeParente.compareTo(autre) || interf != null && interf.compareTo(autre);
    	}
    }
    

    public TDMETH getMethodes() {
    	return this.methodes;
    }



    public TDS<INFOATTRIBUT> getAttributs() {
    	return this.attributs;
    }


	@Override
	public void addAttribut(String nom, INFOATTRIBUT attribut) {
		if (attributs.containsKey(nom)) {
			// TODO : c'est egg qui doit afficher cette erreur
			System.out.println("ERREUR : l'attribut "+nom+" est déjà défini dans la classe "+nom);
			throw new RuntimeException();
		}
		attribut.setDeplacement(size);
		attributs.inserer(nom, attribut);
		size += attribut.getType().getTaille();
	}

	@Override
	public void addMethode(INFOMETHODE methode) {
		methodes.inserer(methode, this);
	}
	

	@Override
	public INFOMETHODE rechercherMethode(String nom, List<ARGUMENT> arguments) {
		return methodes.chercherLocalement(nom, arguments);
	}
	
	@Override
	public INFOMETHODE chercherOptimumMethode (String nom, List<DTYPE> arguments) {
	    try {
		return methodes.chercherOptimum(nom, arguments);
	    }
	    catch (Exception e) {
		return null;
	    }
	}

    // ATTENTION, cette distance n'est pas symmétrique (d(a,b) != d(b,a)) ; il est possible que ça serve
	// autre doit être le type le plus "gros"
    public int distanceDe(DTYPE autre) {
    	if (!(autre instanceof STRUCTURE))
    		return -1;
		if(nom.equals(autre.getNom())) {
		    return 0;
		} 
		else { 
	    	int distClasseParente = -1;
	    	int distInterf = -1;
	    	if (classeParente != null){
	    		distClasseParente = classeParente.distanceDe(autre);
	    		distClasseParente =  distClasseParente<0 ? -1 : 1+distClasseParente;
		    }
	    	if (interf != null){
	    		distInterf = interf.distanceDe(autre);
	    		distInterf =  distInterf<0 ? -1 : 1+distInterf;
		    }
	    	if (distClasseParente == -1)
	    		return distInterf;
	    	else if (distInterf == -1)
	    		return 	distClasseParente;
	    	else
	    		return Math.min(distClasseParente, distInterf); // Si une classe implémente une interface et étend une classe, on prend le minimum des deux distances.
		    
		    	
		}
    }
    
    public String toString() {
//    	return "tot";
        String s =  "\n----------------------------------------------\n"
        		 +"CLASSE : "+nom + "(" + taille + ")\n"
        		 + "----------------------------------------------\n";
        if (this.interf != null)
        	s += "\n\tInterface      : "+this.interf.getNom();
        if (this.classeParente != null)
        	s += "\n\tClasse parente : "+this.classeParente.getNom();
        
        s += "Attributs : \n-------------\n";

        for (String attribut : attributs.keySet()) {
        	s += "\t\t"+attribut + " : "+attributs.get(attribut).getType().getNom()+ "\n";
        }
        
        s += "Méthodes : \n-------------\n";
        s += methodes;
		s += "------------------FIN CLASSE --------------------\n";
        return s;
    }
    
    public boolean extendsRecursifs(CLASSE classe) {
    	while(classe != null) {
    		if(classe.getNom().equals(this.nom)){
    			return true;
    		} else {
    			classe = classe.getParent();
    		}
    	}
    	return false;
    }

	/**
	 * @return the numerosMethodes
	 */
	public ArrayList<INFOMETHODE> getNumerosMethodes() {
		return numerosMethodes;
	}

	/**
	 * @param numerosMethodes the numerosMethodes to set
	 */
	public void setNumerosMethodes(ArrayList<INFOMETHODE> numerosMethodes) {
		this.numerosMethodes = numerosMethodes;
	}

	public void setNumeroClasse(int noClasse) {
		this.noClasse = noClasse;
	}
	
	public int getNumeroClasse() {
		return this.noClasse;
	}
	
	public int getSize() {
		return this.size;
	}

}
