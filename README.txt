Compilation / Execution
-----------------------

    Makefile
        (à la racine)
        $ ant
        $ ./mjcompiler ex_mj/Fact.mj

    Apache-Ant
        (dans src/)
        $ make
        $ ./mjcompiler ../ex_mj/Fact.mj



Usage
-----
    $ ./mjcompiler [Fichier] [Options]


Options
-------
    -h
        Afficher l'aide

    -v 
        Mode verbose, affiche la TDS
